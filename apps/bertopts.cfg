#!/bin/bash
#
# 2d/3d inversion tool bert
# (description of options)
# ------------------------------------------------------
# OPTIONNAME=value    # description (possible values)
#

#
# Global settings
#
VERSION=2.2.9
DATAFILE=notexisting.dat   # data filename (required)
DIMENSION=3      # dimension of the problem (2 for 2d and 3 for 3d)
TOPOGRAPHY=0     # defines if topography is present (0 or 1), overriden by TOPOPOINTS/TOPOPOLY
TOPOPOINTS=      # file with additional coordinates for 3d topography (x y z)
TOPOPOLY=        # file with additional polygons (sep. by blank line) for 3d topo
CYLINDER=        # geometry is closed
TIMESTEPS=       # file with the names of additional datafiles for timelapse inversion
PARAGEOMETRY=    # script (python or bash) creating geometry input (mesh/mesh.poly) or file
PARAMESH=        # mesh or command creating the inversion mesh (mesh/mesh.bms)
INTERFACE=       # file with x/z columns describing (2d) interface(s) (separated by blank line)
ELECTRODENODES=1 # electrodes are represented by nodes (not yet interpreted!)
USEBERT1=0       # uses previously generated BERT 1 meshes, potentials and sensitivity (deprecated)
KEEPPRIMPOT=0    # keep primary potentials on primary mesh, otherwise interpolate on-the-fly
DEBUG=0          # prints even more detailed information and save important temporary files
SAVEALOT=0       # be very verbose and save a lot of files for debugging purposes
PYTHON=python    # command or location of python executable

#
# Data settings (affecting dcedit=filter target before inversion)
#
OVERRIDEERROR=0 # overrides given errors with INPUTERRLEVEL and INPUTERRVOLTAGE
INPUTERRLEVEL=3 # input error level (in percent) if no error given (dcedit -p but still -e)
INPUTERRVOLTAGE=100e-6    # input voltage error (V) if no error given
KMIN=           # minimum geometric factor (not given-> use KMAX as absolute value)
KMAX=9e99       # maximum geometric factor
RMIN=0          # minimum apparent resistivity
RMAX=9e99       # maximum apparent resistivity
IPMIN=-9e99     # minimum IP value 
IPMAX=9e99      # maximum IP value 
ERRMAX=9e99     # maximum error estimate
DOWNWEIGHT=0    # downweight instead of deleting data to keep data structure
ABSRHOA=0       # Force all apparent resistivities to be positive
OMITAUTO_TOPOCORRECTION=0 # Omit automatic topography data correction (if the data are already with correct geometry factors)
#
# Inversion settings
#
CONSTRAINT=1     # constraint type (1/2-first/second order, 0-min.length, 10/20 mix)
LAMBDA=20        # regularization strength
ZWEIGHT=1        # weight for purely vertical gradients after Coscia et al. (2011)
LAMBDAOPT=0      # optimze lambda by using l-curve
CHI1OPT=0        # optimize lambda such that CHI^2=1
REGIONFILE=      # region file for inversion options for each region
BLOCKYMODEL=0    # iteratively (L1) reweighted model roughness
ROBUSTDATA=0     # iteratively (L1) reweighted data misfit
LOWERBOUND=0.0   # lower resistivity bound (logarithmic barrier)
UPPERBOUND=0.0   # upper resistivity bound (0.0 = deactivated)
RECALCJACOBIAN=1 # recalculate jacobian each iteration step (default since version 2)
MAXITER=20       # maximum number of iteration steps
STARTMODEL=      # starting model, either a value (homogeneous model) or a filename holding a vector
RESOLUTIONFILE=  # file with indices/positions to compute resolution kernels for
LAMBDADECREASE=1 # decrease lambda in each iteration by factor (e.g. 0.8)
LAMBDAIP=        # regularization strength for IP inversion (default is LAMBDA)
LOCALREGULARIZATION=0 # local regularization (constrain only model update instead of model)
#SINGVALUE=-1      # potential value at electrodes, for sensitivity (BERT1)
SENSMATDROPTOL=0   # drop tolerance for Jacobian values (sparse storage)
SENSMATMAXMEM=2000 # available memory in MB for pre-allocation of sparse Jacobian
#
# Time lapse inversion settings
#
LAMBDATIMELAPSE=      # separate lambda value for timesteps
TIMELAPSECONSTRAINT=1 # constraint type for TL inversion (see CONSTRAINT)
TIMELAPSEREGIONFILE=  # region file  for TL inversion (see REGIONFILE)
TIMELAPSESTEPMODEL=0  # use preceding model as reference instead of first
FASTTIMELAPSE=0       # no jacobian recalculation for timesteps (only for identical arrays)
RATIOSTEP=0           # very simple (low-contrast) and fast method
TIMELAPSEREMOVEMISFIT=0 # difference inversion after LaBreque & Yang (2001)

#
# Mesh settings
#
PARADEPTH=0           # maximum depth of parameter domain in meter (0 = automatic estimation by 1d coverage)
PARADX=0.0            # refinement for para mesh (values >0.5 will be forced to 0.5)
PARA2DQUALITY=34.0    # parameter grid (from 25 (very bad) to 35 (good))
PARA3DQUALITY=1.3     # parameter grid (from 1.11 (good) to 2 (bad))
PARAMAXCELLSIZE=0     # maximum cell size volume (m3) (DIMENSION=3); area (m2) (DIMENSION=2) for para mesh
PARAEDGELENGTH=0      # compute PARAMAXCELLSIZE by volume of regular triangle/tetrahedron (easier to control)
PARABOUNDARY=5        # boundary around electrodes in parameter domain (percent)
SPLINEBOUNDARY=0      # spline circle boundary instead of piecewise linear interpolation
EQUIDISTBOUNDARY=1    # equidistant refined space between electrodes (2d) (default in BERT2)
SURFACEQUALITY=30     # quality of topographical surface grid (from 20 (bad) to 35 (good))
SURFACEMAXTRISIZE=0.0 # maximal triangle area of paramatric surface grid
SURFACESMOOTH=0       # improve quality of topographical surface grid
# regular grids
GRID=0                # generate regular (quadriliteral or hexahedral) mesh (only TOPOGRAPHY=0, BERT2)
LAYERS=10             # number of layers to use for grid (for GRID=1 only)
PARADZ=1              # layer thickness to use for grid (for GRID=1 only)
# forward mesh
TOTALPOTENTIAL=0      # don't use secondary potential approach (i.e. for CEM meshes or already fine meshes)
SECP2MESH=0           # use quadratic shape functions in forward mesh
OMITSECREFINE=0       # omit global refinement for forward calculation (for fine parameter meshes)
#SECMESHREFINE=1      # the opposite (BERT1)
# primary mesh
PRIMDX=0.1            # electrode refinement (prim mesh) (note: relative in 2d, absolute in 3d)
PRIMDX_R=0.0          # electrode refinement (prim mesh) dr in center direction (overides PRIMDX)
BOUNDARY=500          # size of boundary area around parameter domain
PRIM2DQUALITY=33.4    # primary grid (from 25 (very bad) to 35 (good))
PRIM3DQUALITY=1.2     # primary grid (from 1.11 (good) to 2 (bad))
PRIMMAXCELLSIZE=0     # maximum cell size volume (m3) (DIMENSION=3); area (m2) (DIMENSION=2) for prim mesh
PRIMP2MESH=1          # use primary p2 mesh (default in BERT2)
#ICDROPTOL=0.0         # if number of nodes  200k drop tolerance is set for ICCG solver (BERT1)
# tetgen 3D mesher
MESHGEN=tetgen        # command or location 3d mesh generator
TETGENTOLERANCE=1e-12    # tetgen tolerance limit
TETGENPRESERVEBOUNDARY=0 # tetgen should suppress splitting of boundary facets or segments
LOOPTETGEN=0          # use tetgen itself to slightly improve mesh quality due to repeating tetgen calls

#
# Directory settings (hardly to be changed at all)
#
MESHBASENAME=mesh      # basename for mesh files
DIRMESHS=mesh          # directory name for mesh files
DIRPOT=primaryPot      # directory name for primary and interpolated potentials
DIRPRIMPOT=potentials  # subdirectory name for primary potentials (obsolete)
DIRINTERPOLPOT=interpolated # subdirectory name for interpolated potentials (obsolete)
DIRSENS=sensM          # directory name for sensitivity matrix
OLDPRIMMESHSTYLE=0     # alternative way to create primary mesh (for internal use only)

#
# Plotting settings (so far only for 2d plots with pytripatch)
#
PYTRIPATCH=pytripatch  # command and location of pytripatch (2d mesh plotting)
USECOVERAGE=1          # use coverage for alpha shading
SHOWELECTRODES=1       # show electrode positions as black dots
INTERPERC=3            # use 3% interpercentiles for model display with pytripatch
cMin=                  # minimum for colour scale
cMax=                  # minimum for colour scale
cMap=                  # colour map (default viridis), see matplotlib colorscales (e.g. jet)