// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-VSPo:] file");
  lOpt.setDescription( (string) "Convert plc[*.poly](default) or stl[*.stl] file format into other[vtk/stl/plc] file format" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "VTK-output", no_argument, 'V', "convert into vtk format" );
  lOpt.insert( "STL-output", no_argument, 'S', "convert into stl format" );
  lOpt.insert( "PLC-output", no_argument, 'P', "convert into plc format" );
  lOpt.insert( "output", required_argument, 'o', "output basename" );

  bool help = false, verbose = false;
  bool vtkOutput = false, stlOutput = false, plcOutput = false;
  std::string outputname = NOT_DEFINED;

  double x = 1.0, y = 1.0, z = 1.0;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvVSPo:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'V': vtkOutput = true; break;
    case 'S': stlOutput = true; break;
    case 'P': plcOutput = true; break;
    case 'o': outputname = optarg; break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  bool stlInput = false, plcInput = false;
  string inputSuffix;

  string worldName( argv[ argc - 1] );
  int dimension = 3;

  if ( worldName.find( ".stl" ) != string::npos ) {
    stlInput = true;
    inputSuffix = ".stl";
    dimension = 3;
  } else {
    plcInput = true;
    inputSuffix = ".poly";
    dimension = findDomainDimension( worldName.substr( 0, worldName.rfind( ".poly" ) ) + ".poly" );
  }

  std::string worldFileName( worldName.substr( 0, worldName.rfind( inputSuffix ) ) + inputSuffix );

  std::string outputBase( worldName.substr( 0, worldName.rfind( inputSuffix ) ) );
  if ( outputname != NOT_DEFINED ){ outputBase = outputname;  }

  switch ( dimension ){
  case 2: {
    Domain2D world;
    world.load( worldFileName );
    world.saveVTKUnstructured( outputBase );
  } break;
  case 3: {
    Domain3D world;

    if ( stlInput ) {
      world.loadSTL( worldFileName );
    } else {
      world.load( worldFileName );
    }
    if ( vtkOutput ) world.saveVTK( outputBase );
    if ( stlOutput ) world.saveSTL( outputBase );
    if ( plcOutput ) world.savePLC( outputBase );
  } break;
  default: return -1;
  }


  return 0;
}
