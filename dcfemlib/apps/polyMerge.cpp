// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-d:t:] base-filename object-filename output-filename");
  lOpt.setDescription( (string) "Merge object into base and save to output" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "tolerance", required_argument, 't', "tolerance" );
  lOpt.insert( "no-check", required_argument, 'N', "merge without checking" );

  bool help = false, verbose = false, noCheck = false;
  double tolerance = TOLERANCE;

  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hNvt:",
				       lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 't': tolerance = atof( optarg ); break;
    case 'N': noCheck = true; break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 4 ) { lOpt.printHelp( argv[0] ); return 1; }

  string worldName( argv[ argc-3 ] );
  string objName( argv[ argc-2 ] );
  string outName( argv[ argc-1 ] );

  string worldFileName( worldName.substr( 0, worldName.rfind( ".poly" ) ) + ".poly" );
  string objFileName( objName.substr( 0, objName.rfind( ".poly" ) ) + ".poly" );
  string outFileName( outName.substr( 0, outName.rfind( ".poly" ) ) + ".poly" );

  if ( verbose ){
    cout << "world: " << worldFileName << endl;
    cout << "obj: " << objFileName << endl;
    cout << "out: " << outFileName << endl;
  }

  int dimension = findDomainDimension( worldFileName );

  BaseMesh *world, *obj;
  switch ( dimension ){
  case 2:
    world = new Domain2D();
    obj = new Domain2D();
    break;
  case 3:
    world = new Domain3D( verbose );
    obj = new Domain3D();
    break;
  default: return -1;
  }
  obj->load( objFileName );
  world->load( worldFileName );
  world->merge( *obj, tolerance, !noCheck );
  world->save( outFileName );

  return 0;
}
