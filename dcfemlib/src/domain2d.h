// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef DOMAIN2D__H
#define DOMAIN2D__H DOMAIN2D__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "mesh2d.h"
#include "trianglewrapper.h"
using namespace MyMesh;

#include "facet.h"

class DLLEXPORT Domain2D : public Mesh2D {
public:
  /*! Default contructor. */
  Domain2D();

  /*! Default contructor. */
  Domain2D( const string & fileName );

  /*! Copy contructor. */
  Domain2D( const Domain2D & domain );

  /*! Default destructor. */
  virtual ~Domain2D();

  /*! Assignment operator. */
  virtual Domain2D & operator = ( const Domain2D & domain );

  virtual BaseElement * createEdge( Node & a, Node & b, int marker = 0){
    BaseElement *e = Mesh2D::createEdge( a, b, -1, marker );
    a.insertBoundary( e ); b.insertBoundary( e );
    pFacet_->push_back( Polygon( &a, &b, marker ) );
    return e;
  }
  virtual void clear(){
    pFacet_->clear();
    vecpHoles_.clear();
    BaseMesh::clear();
  }

  /*! Default load method. Load triangle poly format */
  DLLEXPORT virtual int load( const string & filename, int fromone, double snapTol = 1e-12 );
  virtual int load( const string & filename, double snapTol ){ return load( filename, 0, snapTol ); }
  virtual int load( const string & filename ){ return load( filename, 0 ); }

  /*! Default save method. Save triangle poly format */
  DLLEXPORT virtual int save( const string & filename, int fromone );
  virtual int save( const string & filename ){ return save( filename, 0 ); }

  /*! create simple rectangle with default boundary conditions, homog. neumann at surface(max(z)) and mixed BC in subsurface.*/
  void createSimpleMainDomain( const RealPos & start, const RealPos & end );

  void createInterface( const vector < RealPos > & verts, double dx, bool equidistDX, int marker, bool spline, bool close, int vertsMarker = 0);
  void createInterface( const vector < RealPos > & verts, double dx, bool equidistDX, int marker, bool spline, int vertsMarker = 0){
    createInterface( verts, dx, equidistDX, marker, spline, false, vertsMarker );
  }
  void createInterface( const vector < RealPos > & verts, bool close, int vertsMarker = 0){
    createInterface( verts, 0.0, false, 0, false, close, vertsMarker );
  }

  void createCircleMainDomain( const vector < RealPos > & verts, double dx, bool equidistDX, double area, int marker, bool spline, int vertsMarker );
  void createCircleMainDomain( const vector < RealPos > & verts ){ createCircleMainDomain( verts, 0.0, false, 0.0, 0, false, 0 ); }

  void refineElectrodes( const RealPos & dxyz, double dr );
  void refineElectrodes( const RealPos & dxyz ){ refineElectrodes( dxyz, 0.0 ); }
  void refineElectrodes( double dr ){ refineElectrodes( RealPos( 0.0, 0.0, 0.0 ), dr ); }

  /*!Returns a pointer to the Node at position pos is there no Node the method returns NULL.*/
  Node * nodeAt( const RealPos & pos, double snapTol = 1e-10 );

  /*! Add queue of nVerts vertices/nodes to this \ref Domain2D from start to end, each node can be marked with a marker.*/
  void createLineOfVIPs( const RealPos & start, const RealPos & end, int nVerts, int marker = 0 );

  /*! Like \ref createLineOfVIPs, but RealPos start must touch a polygon and the next nVerts nodes will places in fix distance rightandside to the edges of the polygon.*/
  void createLineOfVIPsSurface( const RealPos & start, double spacing, int nVerts, int marker, int surfaceMarker, int direction );


  /*! Create a vector of nVerts RealPos which fits the polygon poly. */
  vector < RealPos > createPolygonInterpolate( const vector < RealPos > & poly,
					       const RealPos & start, double spacing, int nVerts, int direction, bool close );

  virtual Node * createVIP( const RealPos & pos, int marker, double tol );
  virtual Node * createVIP( const RealPos & pos, int marker ){ return createVIP( pos, marker, 1e-10 ); }
  virtual Node * createVIP( const RealPos & pos, double tol = TOLERANCE ){ return createVIP( pos, 0, tol ); }
  void insertNode( Node * node );

  int createMesh( Mesh2D & mesh, double quality, double area, bool verbose = false );

  int createMesh( Mesh2D & mesh, double quality, bool verbose = false ){
    return createMesh( mesh, quality, 0.0, verbose );
  }

  Mesh2D createMesh( double quality, double area, bool verbose = false ){
    Mesh2D mesh; createMesh( mesh, quality, area, verbose ); return mesh;
  }
  Mesh2D createMesh( double quality, bool verbose = false ){
    Mesh2D mesh; createMesh( mesh, quality, verbose ); return mesh;
  }

  int countEdges() const;
  int polygonCount() const { return pFacet_->size(); }
  Polygon & polygon( int i ) const { return (*pFacet_)[ i ]; }
  void addPolygon( Polygon * poly ){ pFacet_->push_back( *poly ); }


  Polygon * createPolygon( const Polygon & poly );

  Polygon * createPolygon( const vector < RealPos > & poly, int marker, bool close );
  /*! Accelerator: see \ref createPolygon( const vector < RealPos > & poly, int marker, bool close ), marker set to 0.*/
  Polygon * createPolygon( const vector < RealPos > & poly, bool close = false ){
    return createPolygon( poly, 0, close );
  }

  virtual void merge( const BaseMesh & subdomain,  double tolerance = TOLERANCE, bool check = true ){
    merge( dynamic_cast< const Domain2D & >( subdomain) , TOLERANCE, check ) ;
    }
  void merge( const Domain2D & subdomain,  double tolerance = TOLERANCE, bool check = true );
  void link( const Domain2D & subdomain );
  void transform( double mat[ 4 ][ 4 ] );

  int holeCount() const { return vecpHoles_.size(); }
  RegionMarker & hole( int i ) const { return *vecpHoles_.at( i ); }
  void createHole( const RegionMarker & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }
  void createHole( const RealPos & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }

  Facet facet() const { return *pFacet_; }
  RealPos norm() const { return pFacet_->norm(); }
  Plane plane() const { return pFacet_->plane(); }
protected:

  Facet * pFacet_;
  vector< RegionMarker * > vecpHoles_ ;

};

DLLEXPORT Domain2D create2DWorld( const RealPos & size, int marker = 1, double area = 0.0 );

#endif // DOMAIN2D__H
/*
$Log: domain2d.h,v $
Revision 1.26  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.25  2007/10/04 13:09:54  carsten
*** empty log message ***

Revision 1.24  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.23  2007/09/18 18:46:22  carsten
*** empty log message ***

Revision 1.22  2007/08/15 20:41:27  thomas
dllexports

Revision 1.21  2007/04/20 12:11:27  carsten
*** empty log message ***

Revision 1.20  2006/08/27 18:58:03  carsten
*** empty log message ***

Revision 1.19  2006/08/21 15:21:12  carsten
*** empty log message ***

Revision 1.18  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.17  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.16  2006/03/02 18:30:20  carsten
*** empty log message ***

Revision 1.15  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.14  2005/12/06 13:57:26  carsten
*** empty log message ***

Revision 1.12  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.11  2005/10/20 14:33:49  carsten
*** empty log message ***

Revision 1.10  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.9  2005/09/12 10:38:36  carsten
*** empty log message ***

Revision 1.8  2005/09/08 19:08:48  carsten
*** empty log message ***

Revision 1.7  2005/09/08 11:25:15  carsten
*** empty log message ***

Revision 1.6  2005/08/30 21:41:17  carsten
*** empty log message ***

Revision 1.5  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.4  2005/08/24 20:22:11  carsten
*** empty log message ***

Revision 1.3  2005/08/22 17:28:09  carsten
*** empty log message ***

Revision 1.2  2005/08/02 13:24:54  thomas
*** empty log message ***

Revision 1.1  2005/07/28 19:43:43  carsten
*** empty log message ***

*/
