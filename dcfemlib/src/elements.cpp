// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "elements.h"
#include "numfunct.h"
#include "line.h"

#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

namespace MyMesh{

#include "setalgorithm.h"

int operator == ( const Edge & e1, const Edge & e2 ){
  if ( ( & e1.nodeA() == & e2.nodeA() ) && ( & e1.nodeB() == & e2.nodeB() ) ) return 1;
  else if ( ( & e1.nodeA() == & e2.nodeB() ) && ( & e1.nodeB() == & e2.nodeA() ) ) return 1;
  return 0;
}
int operator != ( const Edge & e1, const Edge & e2 ){ return !( e1 == e2 ); }

int operator == ( const RealPos & a , const RealPos & b ){
  if ( a.distance( b ) < TOLERANCE ) return 1; else return 0;
}
int operator != ( const RealPos & a , const RealPos & b ){ return !( a == b ); }

int operator == ( const IntPos & a , const IntPos & b ){
  if ( a.x() == b.x() && a.y() == b.y() && a.z() == b.z() ) return 1; else return 0;
}
int operator != ( const IntPos & a , const IntPos & b ){ return !( a == b ); }

vector < RealPos > operator - (const vector < RealPos > & a, const vector < RealPos > & b ){
  vector < RealPos > tmp( a );
  for ( uint i = 0; i< a.size(); i ++ ) tmp[ i ] -= b[ i ];
  return tmp;
}
vector < RealPos > operator + (const vector < RealPos > & a, const vector < RealPos > & b ){
  vector < RealPos > tmp( a );
  for ( uint i = 0; i< a.size(); i ++ ) tmp[ i ] += b[ i ];
  return tmp;
}
vector < RealPos > operator * (const vector < RealPos > & a, const double d ){
  vector < RealPos > tmp( a );
  for ( uint i = 0; i< a.size(); i ++ ) tmp[ i ] *= d;
  return tmp;
}

double operator * ( const std::valarray < double > & a, const RealPos & b ){
double result = 0.0;
  if ( a.size() == 2 ) {
    result = a[ 0 ] * b[ 0 ] + a[ 1 ] * b[ 1 ];
  } else if ( a.size() == 3 ) {
    result = a[ 0 ] * b[ 0 ] + a[ 1 ] * b[ 1 ] + a[ 2 ] * b[ 2 ];
  }
  return result;
  cerr << WHERE_AM_I << "dimensions wrong" << endl;
}

void show ( const SetpTriangleFaces & a ){
  for ( SetpTriangleFaces::const_iterator it = a.begin(); it != a.end(); it++ ){
    cout << (*it)->id() << "\t";
  }
  cout << endl;
}
void show ( const SetpCells & a ){
  for ( SetpCells::const_iterator it = a.begin(); it != a.end(); it++ ){
    cout << (*it)->id() << "\t";
  }
  cout << endl;
}

bool posX_less( const RealPos & a, const RealPos & b ){ return ( a.x() < b.x() ); }
bool posX_more( const RealPos & a, const RealPos & b ){ return ( a.x() > b.x() ); }

bool pPosX_less( RealPos * a, RealPos * b ){ return ( a->x() < b->x() ); }

bool xVari( const vector < RealPos > & electrodeList ){
  double start = electrodeList[ 0 ].x();
  for ( size_t i = 1; i < electrodeList.size(); i ++ ){
    if ( fabs( electrodeList[ i ].x() - start ) > TOLERANCE ) return true;
  }
  return false;
}
bool yVari( const vector < RealPos > & electrodeList ){
  double start = electrodeList[ 0 ].y();
  for ( size_t i = 1; i < electrodeList.size(); i ++ ){
    if ( fabs( electrodeList[ i ].y() - start ) > TOLERANCE ) return true;
  }
  return false;
}
bool zVari( const vector < RealPos > & electrodeList ){
  double start = electrodeList[ 0 ].z();
  for ( size_t i = 1; i < electrodeList.size(); i ++ ){
    if ( fabs( electrodeList[ i ].z() - start ) > TOLERANCE ) return true;
  }
  return false;
}

ostream & operator << ( ostream & str, const Node & n ){
  str << n.pos() << " id = " << n.id() << " Marker = " << n.marker();
  return str;
}
ostream & operator << ( ostream & str, Edge & p ){
  str << const_cast< Edge &>( p );
  return str;
}
ostream & operator << ( ostream & str, const Edge & p ){
  str << "Edge " << p.id()  << ": "
      << p.nodeA().id() << ", " << p.nodeB().id() << "\t";
  if ( p.leftCell() != NULL) str << p.leftCell()->id() << "\t";
  else str << "-1\t";
  if ( p.rightCell() != NULL) str << p.rightCell()->id() << "\t";
  else str << "-1\t";
  str << p.marker();

  return str;
}

ofstream & operator << ( ofstream & str, Edge & p ){
  str << const_cast< Edge &>( p );
  return str;
}
ofstream & operator << ( ofstream & str, const BaseElement & p ){
  for ( int i = 0; i < p.nodeCount(); i ++ ){
    str << p.node(i).id() << "\t";
  }
  return str;
}
ofstream & operator << ( ofstream & str, BaseElement & p ){
  for ( int i = 0; i < p.nodeCount(); i ++ ){
    str << p.node(i).id() << "\t";
  }
  return str;
}
ofstream & operator << ( ofstream & str, const Edge & p ){
  str << p.id()  << "\t"
      << p.nodeA().id() << "\t" << p.nodeB().id() << "\t";
  if ( p.leftCell() != NULL) str << p.leftCell()->id() << "\t";
  else str << "-1\t";
  if ( p.rightCell() != NULL) str << p.rightCell()->id() << "\t";
  else str << "-1\t";
  str << p.marker();

  return str;
}
ofstream & operator << ( ofstream & str, Edge3 & p ){
  str << p.id()  << "\t"
      << p.node( 0 ).id() << "\t" << p.node( 1 ).id() << "\t" << p.node( 2 ).id() << "\t";
  if ( p.leftCell() != NULL) str << p.leftCell()->id() << "\t";
  else str << "-1\t";
  if ( p.rightCell() != NULL) str << p.rightCell()->id() << "\t";
  else str << "-1\t";
  str << p.marker();

  return str;
}

void swap( vector < RealPos > & electrodeList ){
  vector < RealPos > swap;
  for ( int i = electrodeList.size(); i > 0; i-- ){
    swap.push_back( electrodeList[ i - 1 ] );
  }
  electrodeList = swap;
}

int loadPosList( const string & filename, vector < RealPos > & posVector ){
  fstream file; if ( !openInFile( filename, &file ) ) return -1;
  posVector.clear();

  vector < string > row;
  int count = 0;
  while( !file.eof() ){
    row = getNonEmptyRow( file );

    switch ( row.size() ){
    case 2: posVector.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ) ); break;
    case 3: posVector.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) ); break;
    default:
      if ( row.size() > 0 ){
	cerr << WHERE_AM_I << " column: "<< count << ", format unknown: "  << row.size() << endl;
  	for ( uint i = 0; i < row.size(); i ++ ) cout << row[i] << " ";
	cout << endl;
      }
      return -1;
    }
    count++;
  }
  file.close();
  return 1;
}

RealPos averagePosition( const VectorpNodes & nodesVector ){
  RealPos result( 0.0, 0.0, 0.0 );
  for ( int i = 0, imax = nodesVector.size(); i < imax ; i++ ){
    result += nodesVector[ i ]->pos();
  }
  result /= nodesVector.size();
  return result;
}

RealPos averagePosition( const vector < RealPos > & posVector ){
  RealPos result( 0.0, 0.0, 0.0 );
  for ( int i = 0, imax = posVector.size(); i < imax ; i++ ){
    result += posVector[ i ];
  }
  return result /= posVector.size();
}

double jacobianDeterminantWithoutZ( const RealPos & p0, const RealPos & p1, const RealPos & p2) {
  //  cout << n0->pos() << n1->pos() << n2->pos() << endl;
  double x1 = p0.x();
  double x2 = p1.x();
  double x3 = p2.x();
  double y1 = p0.y();
  double y2 = p1.y();
  double y3 = p2.y();
  //  cout << (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1) << endl;
  return (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
}

// double RealPos::angleCos( const RealPos & p ) const {
//** Teubner Taschenbuch der Mathematik 1996, S. 786 cos \phi = ( ab / ( |a||b| ) );
//   cout << (*this) << "\t"
//        << p << "\t"
//        << (*this) * p << "\t"
//        << this->abs() << "\t"
//        << p.abs() <<"\t"
//        << "angleCos(p) = " << ( (*this) * p ) / ( this->abs() * p.abs() ) << endl;
//   return ( (*this) * p ) / ( this->abs() * p.abs() );
// }


// Pos< int >::Pos< int >( const RealPos & p, const MyVec::STLMatrix & wm ){
//   RealPos tmp( p );
//   tmp.transform( wm );
//   x_ = (int)rint( tmp.x() );
//   y_ = (int)rint( tmp.y() );
//   z_ = (int)rint( tmp.z() );
//   valid_ = true;
// }

template <> double RealPos::angle( const RealPos & p, AngleType aType ) const {
  double result = acos( this->dot( p ) / ( this->abs() * p.abs() ) );
  if ( isnan(result) ) {
    //    cout << *this << " " << this->abs() << " " << p << " " << p.abs() << endl;
    result = 0.0;
  }
  switch ( aType ){
  case RAD: return result;
  case DEG: return result * 360.0 / ( 2.0 * PI_ );
  }
  return 0.0;
}
template <> double RealPos::angle( const RealPos & p1, const RealPos & p3, AngleType aType ) const {
  double result = ( p1 - (*this) ).angle( p3 - (*this) );
  switch ( aType ){
  case RAD: return result;
  case DEG: return result * 360.0 / ( 2.0 * PI_ );
  }
  return 0.0;
}

template <> bool RealPos::linearlyDependend( const RealPos & p ){
  double t = x_ / p.x();
  if ( ( fabs( p.y() * t - y_ ) < TOLERANCE ) && ( fabs( p.z() * t - z_ ) < TOLERANCE ) ){
    return true;
  }
  return false;
}

template <> bool RealPos::orthogonal( const RealPos & p ){
  if ( this->scalar( p ) < TOLERANCE ) return true;
  return false;
}

// RealPos RealPos::middlePosition( const RealPos & p ) const {
//   return RealPos( ( p.x() - x_) / 2 + x_, ( p.y() - y_) / 2 + y_, ( p.z() - z_) / 2 + z_);
// }

template <> DLLEXPORT RealPos RealPos::crossProduct( const RealPos & p ) const {
  //    ( a2b3 - a3b2 ) * i + ( a3b1 - a1b3 ) * j + ( a1b2 - a2b1 ) * k;
  double a1 = x(), a2 = y(), a3 = z();
  double b1 = p.x(), b2 = p.y(), b3 = p.z();
  return RealPos( (a2 * b3 - a3 * b2),  ( a3 * b1 - a1 * b3 ) , ( a1 * b2 - a2 * b1 ) );
}

template <> DLLEXPORT RealPos RealPos::norm( const RealPos & p1, const RealPos & p2 ) const {
  RealPos tmp( ( p1 - (*this) ).crossProduct( p2 - (*this) ) );
  return ( tmp / tmp.abs() );
}


template <> DLLEXPORT RealPos RealPos::norm( const RealPos & p1 ) const {
  RealPos result = ( *this + p1 ) / 2.0;
  result.setZ( 1.0 );
  result = result.norm( *this, p1 );
  result.setZ( 0.0 );
  return result;
}

//double RealPos::scalar( const RealPos & p ) const {
//return ( x() * p.x() + y() * p.y() + z() * p.z() );
//}

template <> DLLEXPORT bool RealPos::isClockwise( const RealPos & p1, const RealPos & p3 ) const {
  //** Q&D is noch schrottig die Funktion
  //** see Bronstein p.630 jacobianDeterminant if the tetrahedron this,p1,p3,(0,0,0,0)

  RealPos norm = this->norm( p1, p3 ) ;
  if ( isnan( norm.abs() ) ) {
    return 0;
  }

  //** besser mit der Norm arbeiten
  MyVec::STLVector one( 3 ); one[ 0 ] = 1.0; one[ 1 ] = 1.0; one[ 2 ] = 1.0;
  MyVec::STLMatrix A( 3, 3);
  A[ 0 ]= this->vec();  A[ 1 ] = p1.vec();  A[ 2 ] = p3.vec();

  MyVec::STLMatrix A0( A ); A0.setColumn( 0, one );
  MyVec::STLMatrix A1( A ); A1.setColumn( 1, one );
  MyVec::STLMatrix A2( A ); A2.setColumn( 2, one );

  double distance = 0.0, detA = det( A );
  if ( fabs( det( A ) ) < TOLERANCE ){
    //    cout << A << det(A) << endl;
    distance = 0.0;
  } else {
    distance = detA / ( ::sqrt( ::pow( det( A0 ), 2.0 )
			      + ::pow( det( A1 ), 2.0 )
			      + ::pow( det( A2 ), 2.0 ) ) );
  }
  RealPos p4( 0.0, 0.0, 0.0 );

  if ( fabs(distance) < TOLERANCE){
    //    cout << WHERE_AM_I << "Warnung Abstand d == 0, neuer Bezugspunkt ( -1, -1, -1) kann mist sein. " << endl;
    //     cout << (*this) << p1 << p3 << endl;
//     cout << norm << "\t" << distance << endl;
    p4.setXYZ( -1.0, -1.0, -1.0 );
  }

  double x1 = p1.x(), x2 = x(), x3 = p3.x(), x4 = p4.x();
  double y1 = p1.y(), y2 = y(), y3 = p3.y(), y4 = p4.y();
  double z1 = p1.z(), z2 = z(), z3 = p3.z(), z4 = p4.z();;

  double a = x2 - x1, b = y2 - y1, c = z2 - z1;
  double d = x3 - x1, e = y3 - y1, f = z3 - z1;
  double g = x4 - x1, h = y4 - y1, k = z4 - z1;

  //  cout << a << "\t" << b << "\t" << c << "\t" << d << "\t" << j << "\t" << a << "\t" << a << "\t" << a << "\t" << a << "\t" << a << endl;
    //  cout << a * determinant2x2( e, f, h, k ) - b * determinant2x2( d, f, g, k ) + c * determinant2x2( d, e, g, h )<< endl;
  return (a * determinant2x2( e, f, h, k ) - b * determinant2x2( d, f, g, k ) + c * determinant2x2( d, e, g, h ) ) < 0 ;
}

template <> DLLEXPORT double RealPos::jacobianDeterminant( const RealPos & p1, const RealPos & p3 ) const {
  //** see Bronstein p.630 jacobianDeterminant if the tetrahedron this,p1,p3,(0,0,0,0)
  FUTILE
        return -1.0;
//   RealPos norm = this->norm( p1, p3 ) ;

//   STLVector one( 3 ); one[ 0 ] = 1.0; one[ 1 ] = 1.0; one[ 2 ] = 1.0;
//   STLMatrix A( 3, 3);
//   A[ 0 ]= this->vec();  A[ 1 ] = p1.vec();  A[ 2 ] = p3.vec();

//   STLMatrix A0( A ); A0.setColumn( 0, one );
//   STLMatrix A1( A ); A1.setColumn( 1, one );
//   STLMatrix A2( A ); A2.setColumn( 2, one );

//   double distance = 0.0, detA = det( A );
//   if ( fabs( det( A ) ) < TOLERANCE ){
//     distance = 0.0;
//   } else {
//     distance = detA / ( sqrt( pow( det( A0 ), 2.0 )
// 			      + pow( det( A1 ), 2.0 )
// 			      + pow( det( A2 ), 2.0 ) ) );
//   }
//   RealPos p4( 0.0, 0.0, 0.0 );

//   if ( distance < TOLERANCE){
//     cout << WHERE_AM_I << " Abstand d == 0 " << endl;
//   }

//   double x1 = p1.x(), x2 = x(), x3 = p3.x(), x4 = p4.x();
//   double y1 = p1.y(), y2 = y(), y3 = p3.y(), y4 = p4.y();
//   double z1 = p1.z(), z2 = z(), z3 = p3.z(), z4 = p4.z();;

//   double a = x2 - x1, b = y2 - y1, c = z2 - z1;
//   double d = x3 - x1, e = y3 - y1, f = z3 - z1;
//   double g = x4 - x1, h = y4 - y1, k = z4 - z1;

//   //  cout << a << "\t" << b << "\t" << c << "\t" << d << "\t" << j << "\t" << a << "\t" << a << "\t" << a << "\t" << a << "\t" << a << endl;
//     //  cout << a * determinant2x2( e, f, h, k ) - b * determinant2x2( d, f, g, k ) + c * determinant2x2( d, e, g, h )<< endl;
//   return a * determinant2x2( e, f, h, k ) - b * determinant2x2( d, f, g, k ) + c * determinant2x2( d, e, g, h );
}

// bool RealPos::isWithin( BaseElement & base, double tolerance, bool verbose ) const {
// //   switch ( base.rtti() ){
// //   case MYMESH_EDGE_RTTI:
//   return isWithinEdge( dynamic_cast< Edge & >( base ), tolerance, verbose );
// //   case MYMESH_TRIANGLE_RTTI:
// //     return this->isWithinTri( base, tolerance, verbose ); break;
// //   case MYMESH_TETRAHEDRON_RTTI:
// //     return this->isWithinTet( base, tolerance, verbose ); break;
// //   default:
// //     TO_IMPL; cout << base.rtti() << endl;
// //   }
// }

template <> DLLEXPORT bool RealPos::isWithin( const Edge & edge, double tolerance, bool verbose ) const {
  TO_IMPL
//   Line line( edge.nodeA().pos(), edge.nodeB().pos() );

//   if ( line.touch( (*this) ) == 3 ) return true;
  return false;
}

// inline bool lessThanZero(double  f) {
//   return (unsigned long &)f > 0x80000000U;
// } // lessThanZero
inline bool lessThanZero(double  f) {
  return f < 0.0;
}

template <> bool RealPos::isWithin( const Triangle & tri, double tolerance, bool verbose ) const {
  //http://www.flipcode.org/cgi-bin/fcmsg.cgi?thread_show=9859
  RealPos v0 = tri.node( 0 ).pos();
  RealPos v1 = tri.node( 1 ).pos();
  RealPos v2 = tri.node( 2 ).pos();

  RealPos norm = ( v1 - v0 ).cross( v2 - v0 );

  RealPos v0p = v0 - *this;
  RealPos v1p = v1 - *this;
  RealPos v2p = v2 - *this;

  if ( verbose ) cout << v0p.cross( v1p ).dot( norm ) << " " << lessThanZero( v0p.cross( v1p ).dot( norm ) ) << endl;
  if ( verbose ) cout << v1p.cross( v2p ).dot( norm ) << " " << lessThanZero( v1p.cross( v2p ).dot( norm ) ) << endl;
  if ( verbose ) cout << v2p.cross( v0p ).dot( norm ) << " " << lessThanZero( v2p.cross( v0p ).dot( norm ) ) << endl;

  if ( lessThanZero( v0p.cross( v1p ).dot( norm ) ) ) return false;
  if ( lessThanZero( v1p.cross( v2p ).dot( norm ) ) ) return false;
  if ( lessThanZero( v2p.cross( v0p ).dot( norm ) ) ) return false;

  return true;
}

template <> bool RealPos::isWithin( const Tetrahedron & tet, double tolerance, bool verbose ) const {
  double det = 0.0;
  Node testNode( (*this ) );

  Node *n0 = &tet.node( 0 ), *n1 = &tet.node( 1 );
  Node *n2 = &tet.node( 2 ), *n3 = &tet.node( 3 );

  if ( verbose ) cout << "Check: " << tet.id() << " tol= " << tolerance << endl;

  Tetrahedron testTet( *n0, *n1, *n2, *n3 );
  double D0 = testTet.jacobianDeterminant();
  if ( verbose ) cout << "D0 = " << D0 << endl;
  testTet.setNodes( testNode, *n1, *n2, *n3 );
  double D1 = testTet.jacobianDeterminant();
  if ( verbose ) cout << "D1 = " << D1 << endl;
  if ( ( sign( D1 ) != sign( D0 ) ) ){
    //  if ( fabs( D1 ) < fabs( D0 * tolerance ) || ( sign( D1 ) != sign( D0 ) ) ){
    return false;
  }
  testTet.setNodes( *n0, testNode, *n2, *n3 );
  double D2 = testTet.jacobianDeterminant();
  if ( verbose ) cout << "D2 = " << D2 << endl;
  //  if ( fabs( D2 ) < fabs( D0 * tolerance ) || ( sign( D2 ) != sign( D0 ) ) ){
  if ( sign( D2 ) != sign( D0 ) ){
    return false;
  }
  testTet.setNodes( *n0, *n1, testNode, *n3 );
  double D3 = testTet.jacobianDeterminant();
  if ( verbose ) cout << "D3 = " << D3 << endl;
  if ( ( sign( D3 ) != sign( D0 ) ) ){
    //  if ( fabs( D3 ) < fabs( D0 * tolerance ) || ( sign( D3 ) != sign( D0 ) ) ){
    return false;
  }
  testTet.setNodes( *n0, *n1, *n2, testNode );
  double D4 = testTet.jacobianDeterminant();
  if ( verbose ) cout << "D4 = " << D4 << endl;
  if ( ( sign( D4 ) != sign( D0 ) ) ){
    //  if ( fabs( D4 ) < fabs( D0 * tolerance ) || ( sign( D4 ) != sign( D0 ) ) ){
    return false;
  }

return  true;
  if ( fabs( D0 - D1 - D2 - D3 - D4 ) < fabs( D0*tolerance*1000 ) ) {
    return true;
  }  else{
    cout << "Tet: " << tet.id() << " " << D0 << " - " << D1 + D2 + D3 + D4 <<  " = "
	 << D0 - D1 - D2 - D3 - D4 << " > " << fabs( D0*tolerance*1000 ) << endl;
    //    exit(0);
  }

//   Tetrahedron tet( cell );
//   tet.setNodes( testNode, cell.node( 1 ), cell.node( 2 ), cell.node( 3 ) );
//   det = tet.jacobianDeterminant();
//   if ( det < negtolerance ) return false;

//   tet.setNodes( cell.node( 0 ), testNode, cell.node( 2 ), cell.node( 3 ) );
//   det = tet.jacobianDeterminant();
//   if ( det < negtolerance ) return false;

//   tet.setNodes( cell.node( 0 ), cell.node( 1 ), testNode, cell.node( 3 ) );
//   det = tet.jacobianDeterminant();
//   if ( det < negtolerance ) return false;

//   tet.setNodes( cell.node( 0 ), cell.node( 1 ), cell.node( 2 ), testNode );
//   det = tet.jacobianDeterminant();
//   if ( det < negtolerance ) return false;
}

template <> void RealPos::translate( double tx, double ty, double tz ){
  double mat[ 4 ][ 4 ];
  for ( int i = 0; i < 4; i ++) for ( int j = 0; j < 4; j ++) mat[ i ][ j ] = 0.0;
  for ( int i = 0; i < 3; i ++) mat[ i ][ i ] = 1.0;
  mat[ 0 ][ 3 ] = tx;
  mat[ 1 ][ 3 ] = ty;
  mat[ 2 ][ 3 ] = tz;
  this->transform( mat );
}

template <> void RealPos::rotateX( double phi ){
  double mat[ 4 ][ 4 ] ={{ 1.0, 0.0,        0.0,         0.0},
			 {0.0, cos( phi ), -sin( phi ), 0.0},
			 {0.0, sin( phi ), cos( phi ),  0.0},
			 {0.0, 0.0,        0.0,         0.0}};
  this->transform( mat );
}
template <> void RealPos::rotateY( double phi ){
  double mat[ 4 ][ 4 ] ={{ cos( phi ),  0.0, sin( phi ), 0.0},
			 {0.0,         1.0, 0.0,        0.0},
			 {-sin( phi ), 0.0, cos( phi ), 0.0},
			 {0.0,         0.0, 0.0,        0.0}};
  this->transform( mat );
}
template <> void RealPos::rotateZ( double phi ){
  double mat[ 4 ][ 4 ] ={{ cos( phi ), -sin( phi ), 0.0, 0.0},
			 {sin( phi ), cos( phi ),  0.0, 0.0},
			 {0.0,        0.0,         1.0, 0.0},
			 {0.0,        0.0,         0.0, 0.0}};
  this->transform( mat );
}

template <> void RealPos::rotate( double phiX, double phiY, double phiZ ){
  this->rotateX( phiX );
  this->rotateY( phiY );
  this->rotateZ( phiZ );
}
//**** END RealPos implementation


//**** START BaseElement implementation
BaseElement::BaseElement(){
  _status = NOTREFINED;
  _el = NULL;
  _er = NULL;
}

BaseElement::~BaseElement(){
  _attributesVector.clear();
}

Node & BaseElement::node( int i ){
  assert( i > -1 && (size_t)i < _nodesVector.size() );
  return *_nodesVector[ i ];
}
Node & BaseElement::node( int i ) const {
  assert( i > -1 && (size_t)i < _nodesVector.size() );
  return *_nodesVector[ i ];
}
void BaseElement::setNode( int i, Node & node ){
  assert( i > -1 && (size_t)i < _nodesVector.size() );
  _nodesVector[ i ] = & node;
}

RealPos BaseElement::averagePos() const {
//   RealPos result;
//   for ( int i = 0, imax = _nodesVector.size(); i < imax ; i++ ) result += _nodesVector[ i ]->pos();
//   result /= _nodesVector.size();
  return averagePosition( _nodesVector );
}
RealPos BaseElement::normVector() const {

  //** Q&D;
  DO_NOT_USE
  if ( _nodesVector.size() == 3 ){
    RealPos avPos = this->averagePos();
    return avPos.normOf( _nodesVector[ 0 ]->pos(), _nodesVector[ 1 ]->pos() );
  } else return RealPos( 0, 0, 0);
}
int BaseElement::checkEdge3( int a, int b, int c){
  //** check if c is valid in the middle between a and b
  //**  a--c--b
  double distance = ((node( a ).pos() + node( b ).pos() ) / 2.0).distance( node( c ).pos() );

  double AB = node( a ).pos().distance( node( b ).pos() );

  if ( ( distance / AB ) > 1e-4 ){ //** normalise to the length of ab
    cerr << this << " " << this->id() << " " << this->marker() << endl;
    cerr << node( a ) << " " << node( c ) << " " << node( b ) << endl;
    cerr << WHERE_AM_I << " " << distance << " AB: " << AB << " !" << a << "--" << c << "--" << b
	 << (node( a ).pos() + node( b ).pos() ) / 2.0 << node( c ).pos() << endl;
    return -1;
  }
 return 1;
}

BaseElement * BaseElement::findBoundaryCell( ) {

  SetpCells commonCells;
  switch( rtti() ){
  case MYMESH_EDGE_RTTI:
    commonCells = node( 0 ).cellSet() & node( 1 ).cellSet(); break;
  case MYMESH_EDGE3_RTTI:
    commonCells = node( 0 ).cellSet() & node( 1 ).cellSet(); break;
  case MYMESH_TRIANGLEFACE_RTTI:
    commonCells = node( 0 ).cellSet() & node( 1 ).cellSet() & node( 2 ).cellSet(); break;
  case MYMESH_TRIANGLE6FACE_RTTI:
    commonCells = node( 0 ).cellSet() & node( 1 ).cellSet() & node( 2 ).cellSet(); break;
  default:
    cerr << WHERE_AM_I << " don't know, how to handle rtti " << rtti() << endl;
    exit(0);
  }
  commonCells.erase( this );
  if ( commonCells.size() == 1 ) {
    return (*commonCells.begin() );
  }
  else {
    cerr << WHERE_AM_I << " Warning! More than one or no cell found. " << commonCells.size()
	 << " for face "<< id() << endl;
    set< int > neighboursIdx;
    for ( int i = 0; i < shapeNodeCount(); i ++ ){
      cout << node( i ).id() << endl;
      cout << node( i ).pos() << endl;
      show( node( i ).cellSet() );
    }

    show( commonCells );
  }
  return NULL;
}

//**** END BaseElement implementation


//**** START Node implementation
Node::Node(double x, double y, double z, int id, int marker)
  : marker_(marker) {
  _id = id;
  _pos.setX( x );
  _pos.setY( y );
  _pos.setZ( z );
  //  cout << "create: Node::Node(double x, double y, double z, int id, int marker) " << this->id() << " " << this << endl;
}

// Node::Node(double x, double z, int id, int marker)
//   cerr << WHERE_AM_I << " maybee obsolete " << endl;
// //   : marker_(marker) {
// //   _id = id;
// //   _pos.setX( x );
// //   _pos.setY( 0 );
// //   _pos.setZ( z );
// }

Node::Node( const RealPos & pos, int id, int marker )
  : _pos( pos ), marker_( marker ){
  _id = id;
  //  cout << "create: Node::Node(const RealPos & pos, int id, int marker) " << this->id() << " " << this << endl;
}

Node::Node(const Node & node)
  : _pos( node.pos() ), marker_( node.marker() ){
  _id = node.id();
  //  cout << "create: Node::Node(const Node & node) " << this->id() << " " << this << endl;
}


Node::~Node(){
  //  cout << "delete: Node::~node " << this->id() << " " << this << endl;
  _fromlist.clear();
  //  cout << "from" << endl;
  _tolist.clear();
  //  cout << "to" << endl;
  pFaceSet_.clear();
  //  cout << "faces" << endl;
  pCellSet_.clear();
  //  cout << "cells" << endl;

//   SetpEdges tmp; tmp = _tolist;
//   SetpEdges::iterator it;

//   //** merge �to� and �from� to �tmp�
//   if ( !_fromlist.empty() )
//     for (it = _fromlist.begin(); it != _fromlist.end(); it++) tmp.insert( (*it) );

//   //** delete all edges from �tmp�
//   if ( !tmp.empty() ) {
//     for (it = tmp.begin(); it != tmp.end(); it++) delete (*it);

//     if ( !_fromlist.empty() ){
//       cerr << __FILE__  << __LINE__ << " Irgendwas stimmt hier nicht !!" << endl;
//       _fromlist.clear();
//     }
//     if ( !_tolist.empty() ){
//       cerr << __FILE__  << __LINE__ << " Irgendwas stimmt hier nicht !!" << endl;
//       _tolist.clear();
//     }
//     tmp.clear();
//   }
}

Node & Node::operator = (const Node & node){
  cout << "Node = op dangerus , kopiert nicht alle Pointer " << endl;
  if (this != & node){
    _pos = node.pos();
    _id = node.id();
    marker_ = node.marker();
  }
  return * this;
}

Edge * Node::findEdge(Node & node){
  SetpEdges en1( this->fromSet() + this->toSet() );
  SetpEdges en2( node.fromSet() + node.toSet() );

  //  cout << "findEdge : " << _id << " : " << node.id() << endl;
  //** find common edge between (this <--> node )
  for (SetpEdges::iterator it = en1.begin(); it != en1.end(); it ++){
    for (SetpEdges::iterator its = en2.begin(); its!= en2.end(); its ++){
      if ( (*its) == (*it) ){ return (*its); }
    }
  }
  //  cout << "Edge not found " << endl;
  return NULL;
  // exception-versuch sollte ich ueberdenken  throw Errors::edgeNotFound();
}

double Node::angle(Node & n1, Node & n3 ){
//   RealPos a = n1.pos();
//   RealPos b = n3.pos();
  return this->pos().angle( n1.pos(), n3.pos() );
}

int Node::smooth( int function ){
  //  cout << "Smooth" << _id << "\t" <<  marker_ << endl;

  //** Achtung konkave gebiete koennen entstehen wenn zu festen knoten benachbarte gesmooth werden
  //** aufzeichen -> pruefen -> fixen.
  SetpEdges edgeSet( _tolist + _fromlist); //** all Edges contains this node;

  for ( SetpEdges::iterator it = edgeSet.begin(); it != edgeSet.end(); it ++ ){
    if ( (*it)->marker() != 0 ) { // node at interface or boundary
      return 0;
    }
  }

  SetpNodes neighbourSet;
  SetpTriangles triangleSet;

  double x = 0.0, y = 0.0; //** will be the new coordinates

//   //** START smooth interface node
//   if (marker_ != 0) {
//     //**    not yet implemented
//     return 0;
//     if (marker_ == -99 || marker_ == -999) return 0;

//     for ( SetpEdges::iterator it = edgeSet.begin(); it != edgeSet.end(); it ++ ){
//       if ( (*it)->marker() != 0 ) {
// 	neighbourSet.insert( &( (*it)->nodeA() ) );
// 	neighbourSet.insert( &( (*it)->nodeB() ) );
//       }
//     }
//     neighbourSet.erase( this ); //** the this node is NOT Neighbour :)

//     if ( neighbourSet.size() == 2 ){
//       Node * tmpnodes[2];
//       int count = 0;

//       for (SetpNodes::iterator it = neighbourSet.begin(); it != neighbourSet.end(); it ++){
// 	tmpnodes[ count ] = ( *it );
// 	count ++;
// 	x += ( *it )->x();
// 	y += ( *it )->y();
//       }
//       x /= 2.;
//       y /= 2.;
//       if ( fabs(this->angle( * tmpnodes[0], * tmpnodes[1] ) - PI_) < 1E-5 ){
// 	setXY( x, y );  //** sets the new coordinates
//       }
//     }
//     return 1;
//   }
  //** END smooth interface node

  //** START smooth inner node
  for ( SetpEdges::iterator it = edgeSet.begin(); it != edgeSet.end(); it++ ){
    //** collect all nodes which are neighbours of this node;
    neighbourSet.insert( &( (*it)->nodeA() ) );
    neighbourSet.insert( &( (*it)->nodeB() ) );
  }

  neighbourSet.erase( this ); //** *this is NOT a Neighbour :)

//  cout << "Node " << id() << endl;
//   int counter = 0;

//   for ( SetpNodes::iterator it = neighbourSet.begin(); it != neighbourSet.end(); it ++ ){
//     if ( (*it)->marker() != 0 ) counter ++;
//     //cout << (*it)->id() << "\t" <<  (*it)->marker() << endl;
//   }
//   if ( counter > 2 ) return 0;  //** if one neighbour.marker != 0;

  if ( neighbourSet.size() == 0 ) {
    cout << WHERE_AM_I << this->id() << " " << *this << endl;
    return 0;
  }
  x = 0.0; y = 0.0;
  switch ( function ){ //** switch smoothfunction
  case 0:{ //** Summarize all Positions (Laplace)
    for (SetpNodes::iterator it = neighbourSet.begin(); it != neighbourSet.end(); it ++){
      //** sumarize alle koordinates
      x += ( *it )->x();
      y += ( *it )->y();
    }
    //** new coordinates are the average of all neighbour-coordinates
    x /= neighbourSet.size();
    y /= neighbourSet.size();
  } break;
  case 1:{ //** Summarize all Positions (mod. Laplace) with weighting
    double sumweight = 0;
    double * weight = new double[ pCellSet_.size() ];
    int i = 0;
    for (SetpCells::iterator it = pCellSet_.begin(); it != pCellSet_.end(); it++, i++){
      //** collect the areas of al Triangles
      weight[ i ] = (*it)->area();
      //** summarize all the areas
      sumweight += weight[ i ];
    }
    i = 0;
    double xsp = 0.0, ysp = 0.0;
    x = 0.0; y = 0.0;
    for (SetpCells::iterator it = pCellSet_.begin(); it != pCellSet_.end(); it++, i++){
      xsp = (*it)->node(0).x() + (*it)->node(1).x() + (*it)->node(2).x();
      ysp = (*it)->node(0).y() + (*it)->node(1).y() + (*it)->node(2).y();
      x += xsp / 3.0 * weight[i];
      y += ysp / 3.0 * weight[i];
    }
    x /= sumweight;
    y /= sumweight;
    delete[] weight; //** delete unused array
  } break;
  default:
    cerr << WHERE << " Nodemoving function: " << function << " undefined. " << endl;
    return 0;
  }

  setXY( x, y );  //** sets the new coordinates
  return 1;
}

double Node::distance( const Node & node ){
  RealPos p = node.pos();
  return _pos.distance( p );
}
double Node::distanceTo( const Node & node ){
  return distance( node );
}

SetpBaseElements Node::neighbourElements(){
  return pCellSet_;
}

SetpNodes Node::neighbourNodes(){
  SetpNodes neighbourSet;

  for ( SetpCells::iterator it = pCellSet_.begin(); it != pCellSet_.end(); it++ ){
    for ( int i = 0; i < (*it)->nodeCount(); i ++ ){
      neighbourSet.insert( & (*it)->node( i ) );
    }
  }

//   for (SetpEdges::iterator it = edgeSet.begin(); it != edgeSet.end(); it++){
//     neighbourSet.insert( (*it)->pNodeA() );
//     neighbourSet.insert( (*it)->pNodeB() );
//   }
//   SetpEdges edgeSet; edgeSet = merge( toSet(), fromSet() );

  neighbourSet.erase( this );
  return neighbourSet;
}

Edge::Edge(Node & a, Node & b, int id, int marker) {
  //  _nodea( & a ), _nodesVector[1]( & b ){
  //  cout << "Construct Edge " << this << "\t" << a->id() << "\t" << b->id() << endl;
  _nodesVector.push_back( &a );
  _nodesVector.push_back( &b );

  marker_ = marker;
  _id = id;
  a.addFromEdge( * this );
  b.addToEdge( * this );
  _el = NULL;
  _er = NULL;
}

Edge::~Edge(){
  _nodesVector.clear();
//   _nodea->eraseFromEdge( * this );
//   _nodesVector[1]->eraseToEdge( * this );

//   if (_el != NULL) delete _el;
//   if (_er != NULL) delete _er;
}

Edge::Edge( const Edge & edge ){
  cout << "Edge (edge) to dangerus use Edge(*node, *node, id, marker)" << endl;
}

Edge & Edge::operator = ( const Edge & edge ){
  cout << "Edge = op dangerus , kopiert nicht alle Pointer " << endl;
  if ( this != & edge ){
  }
  return * this;
}

void Edge::setNodeA(Node & a){
  _nodesVector[ 0 ]->eraseFromEdge( * this );
  _nodesVector[ 0 ] = & a;
  _nodesVector[ 0 ]->addFromEdge( * this );
}

void Edge::setNodeB(Node & b){
  _nodesVector[ 1 ]->eraseToEdge( * this );
  _nodesVector[ 1 ] = & b;
  _nodesVector[ 1 ]->addToEdge( * this );
}

void Edge::setNodes(Node & a, Node & b){
  setNodeA( a );
  setNodeB( b );
}

double Edge::length() const {
  return _nodesVector[ 0 ]->distance( *_nodesVector[ 1 ] );
}

double Edge::rise(){
  if (fabs(_nodesVector[1]->x() - _nodesVector[0]->x() ) < 1E-20 ) return 123456789;
  return ( _nodesVector[1]->y() - _nodesVector[0]->y() ) / (_nodesVector[1]->x() - _nodesVector[0]->x() );
}

double Edge::offset(){
  return _nodesVector[0]->y() - ( _nodesVector[0]->x() * rise() );
}

double Edge::partDerivationRealToUnity( char absPos, int relPos ){
  TO_IMPL;
  return -1;
}

RealPos Edge::normVector() const{
  return _nodesVector[ 0 ]->pos().norm( _nodesVector[ 1 ]->pos() );
}

RealPos Edge::intersectionPos(Edge & edge){
//   double a = rise();
//   double b = offset();
//   double c = edge.rise();
//   double d = edge.offset();

  double x = 0.0, y = 0.0;
  RealPos intersect;

//   if (a > 123456789) {
//     x = _nodesVector[0]->x();   //** if one of these edge parallel to abscissa
//     z = x * c + d;
//   } else if (c > 123456789){
//     x = edge.nodeA().x();
//     z = x * a + b;
//   } else if ( fabs(a - c) < (double)(1E-10) ){     //** if both edges parallel
//     cerr << "Edges Parallel a -c " << a - c << endl;
//     //    intersect.setValid(0);
//   } else {
//     x = (d - b) / (a - c);
//     z = x * a + b;
//     //    intersect.setValid( 1 );
//   }

// //   cout << "a" << a << "\tb " << b << "\tc "<< c << "\td "<< d
// //        << "\tx " << x << "\tx * a + b " << x * a + b << endl;

//   intersect.setX( x );
//   intersect.setY( y );

  cerr << WHERE_AM_I << " use new functions in mesh3dtools " << endl;
  return intersect;
}

void Edge::eraseElement( BaseElement & ele ){
  //  cout << "eraseEdge " << this << "\tElement" << & ele << endl;
  //  cout << _el << "\t" << _er << "\t" << endl;
  if ( _el == & ele ) _el = NULL;
  else if ( _er == & ele ) _er = NULL;
}

int Edge::touch( const RealPos & pos ) const{
  return Line( nodeA().pos(), nodeB().pos() ).touch( pos );
}

int Edge::swap( int babuskacheck ){
  if ( (marker_ != 0 ) || ( _el == NULL ) || ( _er == NULL ) ) {  return 0;  }
  else if ( (_el->rtti() == MYMESH_TRIANGLE_RTTI ) && (_er->rtti() == MYMESH_TRIANGLE_RTTI ) ) {
    /* Edgeswapp only in TriangleMesh */

    Node * oA = _nodesVector[ 0 ];
    Node * oB = _nodesVector[ 1 ];
    Node * oL = & dynamic_cast< Triangle * >( _el )->oppositeTo( * this );
    Node * oR = & dynamic_cast< Triangle * >( _er )->oppositeTo( * this );

//     cerr << " Edge " << id() << " wrong swapped " << endl;
//     cerr << "LeftElement: " << _el->id() << "; RigthElement: " << _er->id() << endl;
//     cerr << "NodeA: " << oA->id() << ", NodeB: " << oB->id()
// 	 << ", NodeL: " << oL->id() << ", NodeR: " << oR->id() << endl;

    double aa = oA->angle( * oL, * oR );
    double ab = oB->angle( * oL, * oR );
    double al = oL->angle( * oA, * oB );
    double ar = oR->angle( * oA, * oB );

    //** returns 0 if the sum of all 4 inner angles lower then 360 �
    if ( aa + ab + al + ar < 2 * PI_ ) return 0;
    if ( babuskacheck == 1 ){
      if ( al + ar < PI_ ) return 0;
    }

    setNodeA( * oL );
    setNodeB( * oR );

    if (_el == _er){
      cerr << WHERE << " Edge " << id() << " wrong swapped " << endl;
      cerr << "LeftElement: " << _el->id() << "; RigthElement: " << _er->id() << endl;
      cerr << "NodeA: " << oA->id() << ", NodeB: " << oB->id()
	   << ", NodeL: " << oL->id() << ", NodeR: " << oR->id() << endl;
      return 0;
    }

    dynamic_cast< Triangle * >( _er )->setNodes( * oL, * oA, * oR);
    dynamic_cast< Triangle * >( _el )->setNodes( * oL, * oR, * oB);

  } else return 0;
  return 1;
}
//**** END Edge implementation


Edge3::Edge3( Node & a, Node & b, Node & c, int id, int marker )
: Edge(a, b, id, marker){
  _nodesVector.push_back( &c );
}

Edge3::~Edge3(){
}

//**** START Triangle implementation
Triangle::Triangle(Node & n1, Node & n2, Node & n3, int id, double attribute, bool registerInMesh)
  : BaseElement(){
  _id = id;

  if (_attributesVector.size() == 0) _attributesVector.push_back(attribute);
  else _attributesVector[ 0 ] = attribute;

  _nodesVector.clear();
  _nodesVector.push_back( & n1 );
  _nodesVector.push_back( & n2 );
  _nodesVector.push_back( & n3 );

//   n1.insertCell( this );
//   n2.insertCell( this );
//   n3.insertCell( this );
  //** Q&D
  if ( registerInMesh ) setNodes( n1, n2, n3 );
}

Triangle::Triangle( const Triangle & triangle ){
//   _id = triangle.id();
//   _attribute = triangle.attribute();
  cerr << __ASSERT_FUNCTION << " should not be used." << endl;
}

Triangle::~Triangle(){
  //  for (int i = 0; i < 3; i++) _edges[ i ]->eraseElement( * this );
}

void Triangle::setNodes(Node & n1, Node & n2, Node & n3){
  _nodesVector[0] = & n1, _nodesVector[1] = & n2, _nodesVector[2] = & n3 ;

//   n1.insertCell( this );
//   n2.insertCell( this );
//   n3.insertCell( this );

  if ( this->jacobianDeterminant() < 0 ) { _nodesVector[ 1 ] = & n3; _nodesVector[ 2 ] = & n2; }

  VectorpEdges eds = collectEdges( *_nodesVector[0], *_nodesVector[1], *_nodesVector[2] );

  if ( eds.size() > 2){
    //** define if triangle left or right sided from every edge
    Node * node;
    for (int i = 0; i < 3; i ++) {
      _edges[i] = eds[i];
      if (i == 0 ) node = _nodesVector[2]; else node = _nodesVector[ i - 1 ];

      //     cout << _edges[i].id() << "NodeA" << _edges[i]->nodeA().id() << endl;
      //     cout << _edges[i].id() << "NodeB" << _edges[i]->nodeB().id() << endl;
      //     cout << "Node" << node().id() << endl;

      if ( jacobianDeterminant( & _edges[i]->nodeA(), & _edges[i]->nodeB(), node) > 0){
	_edges[i]->setLeftCell( * this );
      } else _edges[i]->setRightCell( * this );
    }
  }
}

VectorpEdges Triangle::collectEdges(Node & n1, Node & n2, Node & n3){
  VectorpEdges eds;
  Edge * edge;
  if ( ( edge = n1.findEdge( n2 ) ) != NULL ) eds.push_back( edge );
  if ( ( edge = n2.findEdge( n3 ) ) != NULL ) eds.push_back( edge );
  if ( ( edge = n3.findEdge( n1 ) ) != NULL ) eds.push_back( edge );
  return eds;
}


Triangle & Triangle::operator = (const Triangle & triangle){
  cout << "Triangle = op dangerus , kpoiert nicht alle Pointer " << endl;
  if (this != & triangle){  }
  return * this;
}

Edge & Triangle::edge(int i) const {
  assert( i > -1 && i < 3 );
  return * _edges[ i ];
}

MyVec::STLMatrix Triangle::jacobianMat( ) const {
  double x0 = _nodesVector[ 0 ]->x(), y0 = _nodesVector[ 0 ]->y();
  double x1 = _nodesVector[ 1 ]->x(), y1 = _nodesVector[ 1 ]->y();
  double x2 = _nodesVector[ 2 ]->x(), y2 = _nodesVector[ 2 ]->y();

  MyVec::STLMatrix J( 2, 2 );
  J[ 0 ][ 0 ] = x1 - x0; J[ 0 ][ 1 ] = x2 - x0;
  J[ 1 ][ 0 ] = y1 - y0; J[ 1 ][ 1 ] = y2 - y0;

  return J;
}

double Triangle::jacobianDeterminant(Node * n0, Node * n1, Node * n2) const {
  if ( n0 == NULL ) n0 = _nodesVector[ 0 ];
  if ( n1 == NULL ) n1 = _nodesVector[ 1 ];
  if ( n2 == NULL ) n2 = _nodesVector[ 2 ];
  double x0 = n0->x(), y0 = n0->y();
  double x1 = n1->x(), y1 = n1->y();
  double x2 = n2->x(), y2 = n2->y();
  return ( x1 - x0 ) * ( y2 - y0 ) - ( x2 - x0 ) * ( y1 - y0 );
}

Node & Triangle::oppositeTo(Edge & edge){
  for (int i=0; i<3; i++)
    if ( ( & edge.nodeA() != _nodesVector[i] ) && ( & edge.nodeB() != _nodesVector[i] ) )
      return *_nodesVector[i];
  return *_nodesVector[2];
}

double Triangle::partDerivationRealToUnity( char absPos, int relPos ){
  TO_IMPL;
  return -1;
}

Triangle6::Triangle6( Node & n1, Node & n2, Node & n3, int id, double attribute )
  : Triangle( n1, n2, n3, id, attribute ){

  for ( int i = 0; i < 3 ; i++ ) _nodesVector.push_back( NULL );
}
Triangle6::~Triangle6(){}

void Triangle6::addNodeFromEdge3( Edge3 & edge ){
  Node * pNode = & edge.node( 2 );

  if ( &edge.node( 0 ) == &node( 0 ) && &edge.node( 1 ) == &node( 1 ) ) setNode( 3, *pNode );
  else if ( &edge.node( 0 ) == &node( 1 ) && &edge.node( 1 ) == &node( 0 ) ) setNode( 3, *pNode );
  else if ( &edge.node( 0 ) == &node( 1 ) && &edge.node( 1 ) == &node( 2 ) ) setNode( 4, *pNode );
  else if ( &edge.node( 0 ) == &node( 2 ) && &edge.node( 1 ) == &node( 1 ) ) setNode( 4, *pNode );
  else if ( &edge.node( 0 ) == &node( 2 ) && &edge.node( 1 ) == &node( 0 ) ) setNode( 5, *pNode );
  else if ( &edge.node( 0 ) == &node( 0 ) && &edge.node( 1 ) == &node( 2 ) ) setNode( 5, *pNode );
}

void Triangle6::check( ){
  Node *p3 = _nodesVector[ 3 ] ;
  Node *p4 = _nodesVector[ 4 ] ;
  Node *p5 = _nodesVector[ 5 ] ;

  if ( ( ( node(0).pos() + node(1).pos() )/ 2.0)  == p3->pos() ) _nodesVector[ 3 ] = p3;
  else if ( ( ( node(0).pos() + node(1).pos() ) / 2.0 ) == p4->pos() ) _nodesVector[ 3 ] = p4;
  else if ( ( ( node(0).pos() + node(1).pos() ) / 2.0 ) == p5->pos() ) _nodesVector[ 3 ] = p5;

  if ( ( ( node(0).pos() + node(2).pos() ) / 2.0 ) == p3->pos() ) _nodesVector[ 4 ] = p3;
  else if ( ( ( node(0).pos() + node(2).pos() ) / 2.0 ) == p4->pos() ) _nodesVector[ 4 ] = p4;
  else if ( ( ( node(0).pos() + node(2).pos() ) / 2.0 ) == p5->pos() ) _nodesVector[ 4 ] = p5;

  if ( ( ( node(1).pos() + node(2).pos() ) / 2.0 ) == p3->pos() ) _nodesVector[ 5 ] = p3;
  else if ( ( ( node(1).pos() + node(2).pos() ) / 2.0 ) == p4->pos() ) _nodesVector[ 5 ] = p4;
  else if ( ( ( node(1).pos() + node(2).pos() ) / 2.0 ) == p5->pos() ) _nodesVector[ 5 ] = p5;

  //  cerr << this << " " << this->id() << " " << this->marker () << endl;
  checkEdge3( 0, 1, 3); // edge 0 -- 4 -- 1
  checkEdge3( 0, 2, 4);	// edge 0 -- 5 -- 2
  checkEdge3( 1, 2, 5);	// edge 0 -- 6 -- 3
}

TriangleFace::TriangleFace( Node & n1, Node & n2, Node & n3, int id, int marker )
: Triangle( n1, n2, n3, id, marker, false) {
  marker_ = marker;
  _leftNeighbour = NULL;
  _rightNeighbour = NULL;
}

TriangleFace::~TriangleFace(){
  //  cout << WHERE_AM_I << endl;
}

double TriangleFace::area( ) const {
  RealPos p1 = _nodesVector[ 0 ]->pos();
  RealPos p2 = _nodesVector[ 1 ]->pos();
  RealPos p3 = _nodesVector[ 2 ]->pos();

  return ( ( p2 - p1 ).crossProduct( p3 - p1 ) ).abs() / 2.0;
}
double TriangleFace::jacobianDeterminant( ) const {
  return 2 * area( );
}

RealPos TriangleFace::normVector( ) const {
  RealPos result( averagePos() );
  return result.normOf( _nodesVector[ 0 ]->pos(), _nodesVector[ 1 ]->pos() );
}

Triangle6Face::Triangle6Face( Node & n1, Node & n2, Node & n3, int id, int marker )
: Triangle6( n1, n2, n3, id, marker ) {
  marker_ = marker;
}
Triangle6Face::~Triangle6Face(){
}

double Triangle6Face::area( ) const {
  RealPos p1 = _nodesVector[ 0 ]->pos();
  RealPos p2 = _nodesVector[ 1 ]->pos();
  RealPos p3 = _nodesVector[ 2 ]->pos();

  return ( ( p2 - p1 ).crossProduct( p3 - p1 ) ).abs() / 2.0;
}
double Triangle6Face::jacobianDeterminant( ) const {
  return 2 * area( );
}
RealPos Triangle6Face::normVector( ) const {
  RealPos result( averagePos() );
  return result.normOf( _nodesVector[ 0 ]->pos(), _nodesVector[ 1 ]->pos() );
}

//**   Start Quadrangle implementation   **//
Quadrangle::Quadrangle( Node & n1, Node & n2, Node & n3, Node & n4, int id , double attribute )
  : BaseElement(){
  _id = id;

  if (_attributesVector.size() == 0) _attributesVector.push_back(attribute);
  else _attributesVector[0] = attribute;

  _nodesVector.clear();
  _nodesVector.push_back( & n1 );
  _nodesVector.push_back( & n2 );
  _nodesVector.push_back( & n3 );
  _nodesVector.push_back( & n4 );

}

Quadrangle::Quadrangle( Quadrangle & quad ){
  cerr << __ASSERT_FUNCTION << " should not be used." << endl;
}

Quadrangle & Quadrangle::operator = ( Quadrangle & quad ){
  cerr << __ASSERT_FUNCTION << " should not be used." << endl;
  return * this;
}

Quadrangle::~Quadrangle(){

}

double Quadrangle::partDerivationRealToUnity( char absPos, int relPos ){
  TO_IMPL;
  return -1;
}

void Quadrangle::setNodes(Node & n1, Node & n2, Node & n3, Node & n4){

  _nodesVector[0] = & n1, _nodesVector[1] = & n2, _nodesVector[2] = & n3 ;
  _nodesVector[3] = & n4;
  if ( this->jacobianDeterminant() < 0 ) { cerr << __ASSERT_FUNCTION
						<< "Parralelogramm schein links rum :( " << endl;
  }
  VectorpEdges eds = collectEdges( *_nodesVector[0], *_nodesVector[1], *_nodesVector[2], *_nodesVector[3]);


  //** define if quadrangle left or right sided from every edge
  Node * node;
  for (int i = 0; i < 4; i ++) {
    _edges[i] = eds[i]; //* nearly obsolete
    if (i == 0 ) node = _nodesVector[3]; else node = _nodesVector[ i - 1 ];

    TO_CHECK
    TO_IMPL
    //     if ( jacobianDeterminant( & eds[i]->nodeA(), & eds[i]->nodeB(), node) > 0){
    //       eds[i]->setLeftCell( * this );
    //     } else eds[i]->setRightCell( * this );
//      cout << "this "<< this->id() << "\t" << this
//  	  << " Edge " << eds[i]->id() << "\t" << eds[i] << "\t"
//  	  << eds[i]->leftCell() << "\t"  << eds[i]->rightCell() << endl;
  }
}

VectorpEdges Quadrangle::collectEdges(Node & n0, Node & n1, Node & n2, Node & n3){
  VectorpEdges eds;
  eds.push_back( n0.findEdge( n1 ) );
  eds.push_back( n1.findEdge( n2 ) );
  eds.push_back( n2.findEdge( n3 ) );
  eds.push_back( n3.findEdge( n0 ) );

  return eds;
}

RealPos Quadrangle::middlePosition(){
  Node node0( _nodesVector[0]->middlePosition( * _nodesVector[1] ) );
  Node node1( _nodesVector[1]->middlePosition( * _nodesVector[2] ) );
  Node node2( _nodesVector[2]->middlePosition( * _nodesVector[3] ) );
  Node node3( _nodesVector[3]->middlePosition( * _nodesVector[0] ) );

  Edge edge1(node0, node2);
  Edge edge2(node1, node3);

  return edge1.intersectionPos( edge2 );
}

double Quadrangle::jacobianDeterminant( ) const {
  TO_CHECK
  double x1 = _nodesVector[ 0 ]->x();
  double x2 = _nodesVector[ 1 ]->x();
  double x3 = _nodesVector[ 2 ]->x();
  double y1 = _nodesVector[ 0 ]->y();
  double y2 = _nodesVector[ 1 ]->y();
  double y3 = _nodesVector[ 2 ]->y();

  return (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
}

QuadrangleFace::QuadrangleFace( Node & n1, Node & n2, Node & n3, Node & n4, int id, int marker )
  : Quadrangle( n1, n2, n3, n4, id, 0.0 ) {
  marker_ = marker;
  leftNeighbour_ = NULL;
  rightNeighbour_ = NULL;
}

Tetrahedron::Tetrahedron( Node & n1, Node & n2, Node & n3, Node & n4, int id, double attribute )
  : BaseElement() {
  _id = id;
  if ( _attributesVector.size() == 0) _attributesVector.push_back(attribute);
  else _attributesVector[0] = attribute;
  //    _nodesVector.reserve( 4 );
  _nodesVector.clear();
  _nodesVector.push_back( & n1 );
  _nodesVector.push_back( & n2 );
  _nodesVector.push_back( & n3 );
  _nodesVector.push_back( & n4 );
  for ( int i = 0; i < 4; i ++ ) _face[ i ] = NULL;
}

RealPos Tetrahedron::middlePosition(){
  return averagePosition( _nodesVector );
}

void Tetrahedron::moveTo( RealPos & newpos ){
//   RealPos oldpos = middlePosition();
//   for (int i = 0, imax = 3; i< imax; i++ ){
//     _nodesVector[i]->setX( _nodesVector[i]->x() + (oldpos.x() - newpos.x())/3 );
//     _nodesVector[i]->setY( _nodesVector[i]->y() + (oldpos.y() - newpos.y())/3 );
//     _nodesVector[i]->setZ( _nodesVector[i]->z() + (oldpos.z() - newpos.z())/3 );
//   }
//   oldpos = middlePosition();
}

void Tetrahedron::shrink( double shrink ){
//   RealPos center = middlePosition();

//   double transform[4][4]={ {shrink, 0, 0, 0},
// 			   {0, shrink, 0, 0},
// 			   {0, 0, shrink, 0},
// 			   {0, 0, 0, 0} };

//   RealPos pos;
//   //  cout << "old: " <<center.x() << "\t" << center.y() << "\t" << center.z() << endl;
//   for ( int i = 0, imax = 4; i < imax; i++ ){
//     pos = _nodesVector[i]->pos();  pos.transform( transform );
//     _nodesVector[i]->setPos( pos );
//   }
//   //  cout << "new: "<< middlePosition().x() << "\t" << middlePosition().y() << "\t" << middlePosition().z() << endl;
//   RealPos tmp = middlePosition();

//   transform[0][0] = 1.;
//   transform[1][1] = 1.;
//   transform[2][2] = 1.;
//   transform[3][0] = center.x() - tmp.x();
//   transform[3][1] = center.y() - tmp.y();
//   transform[3][2] = center.z() - tmp.z();

//   for ( int i = 0, imax = 4; i < imax; i++ ){
//     pos = _nodesVector[i]->pos();  pos.transform( transform );
//     _nodesVector[i]->setPos( pos );
//   }

//  tmp = middlePosition();
 //  cout << "Distance " << center.distance( tmp ) << endl;
}

void Tetrahedron::scale( double scale ){

//   RealPos centerold = middlePosition();
//   for (int i = 0, imax = 3; i< imax; i++ ){
//     _nodesVector[i]->setX( _nodesVector[i]->x() * scale );
//     _nodesVector[i]->setY( _nodesVector[i]->y() * scale );
//     _nodesVector[i]->setZ( _nodesVector[i]->z() * scale );
//   }
//   moveTo( centerold );
}

double Tetrahedron::jacobianDeterminant( ) const {
//   double x1 = _nodesVector[ 0 ]->x(), x2 = _nodesVector[ 1 ]->x(), x3 = _nodesVector[ 2 ]->x(), x4 = _nodesVector[ 3 ]->x();
//   double y1 = _nodesVector[ 0 ]->y(), y2 = _nodesVector[ 1 ]->y(), y3 = _nodesVector[ 2 ]->y(), y4 = _nodesVector[ 3 ]->y();
//   double z1 = _nodesVector[ 0 ]->z(), z2 = _nodesVector[ 1 ]->z(), z3 = _nodesVector[ 2 ]->z(), z4 = _nodesVector[ 3 ]->z();

//   cout << endl;
//   cout << "1\t" << x1 << "\t" << y1 << "\t" << z1 << endl;
//   cout << "1\t" << x2 << "\t" << y2 << "\t" << z2 << endl;
//   cout << "1\t" << x3 << "\t" << y3 << "\t" << z3 << endl;
//   cout << "1\t" << x4 << "\t" << y4 << "\t" << z4 << endl;

  //** maple wars ;
//   double detOld = y3*z4*x2 - z3*y4*x2 - y2*x3*z4 + z3*y2*x4 + z2*x3*y4 - y3*z2*x4 - y3*z4*x1 + z3*y4*x1 +
//     y2*z4*x1 - z3*y2*x1 - z2*y4*x1 + y3*z2*x1 + y1*x3*z4 - z3*y1*x4 - x2*y1*z4 + z3*x2*y1 +
//     z2*y1*x4 - y1*x3*z2 - z1*x3*y4 + y3*z1*x4 + x2*z1*y4 - y3*x2*z1 - y2*z1*x4 + z1*x3*y2;

  double detNew = det( this->jacobianMat() );

//   if ( fabs( detOld - detNew )/(detOld+1e-10) > 1e-8  ){
//     cerr << WHERE_AM_I << " " << detOld << " " << detNew << "  "<<fabs( detOld - detNew )/(detOld+1e-10)<< endl;
//   }
  return detNew;
}

MyVec::STLMatrix Tetrahedron::jacobianMat( ) const {
  double x0 = _nodesVector[ 0 ]->x(), y0 = _nodesVector[ 0 ]->y(), z0 = _nodesVector[ 0 ]->z();
  double x1 = _nodesVector[ 1 ]->x(), y1 = _nodesVector[ 1 ]->y(), z1 = _nodesVector[ 1 ]->z();
  double x2 = _nodesVector[ 2 ]->x(), y2 = _nodesVector[ 2 ]->y(), z2 = _nodesVector[ 2 ]->z();
  double x3 = _nodesVector[ 3 ]->x(), y3 = _nodesVector[ 3 ]->y(), z3 = _nodesVector[ 3 ]->z();

  MyVec::STLMatrix J( 3, 3 );
  J[ 0 ][ 0 ] = x1 - x0; J[ 0 ][ 1 ] = x2 - x0; J[ 0 ][ 2 ] = x3 - x0;
  J[ 1 ][ 0 ] = y1 - y0; J[ 1 ][ 1 ] = y2 - y0; J[ 1 ][ 2 ] = y3 - y0;
  J[ 2 ][ 0 ] = z1 - z0; J[ 2 ][ 1 ] = z2 - z0; J[ 2 ][ 2 ] = z3 - z0;

  return J;
}

void Tetrahedron::switchDirection(){
  Node * nodetmp = NULL;
  nodetmp = _nodesVector[ 3 ];
  _nodesVector[ 3 ] = _nodesVector[ 2 ];
  _nodesVector[ 2 ] = nodetmp;
}

double Tetrahedron::partDerivationRealToUnity( char absPos, int relPos ){
  //  cerr << WHERE_AM_I << "was macht die funktschon ?? " << endl;
  double result = 0.0;
  switch( absPos ){
  case 'x':
    result = node( relPos ).x() - node( 0 ).x();
    break;
  case 'y':
    result = node( relPos ).y() - node( 0 ).y();
    break;
  case 'z':
    result = node( relPos ).z() - node( 0 ).z();
    break;
  default : cerr << WHERE_AM_I << " Dimension invalid. " << endl;
  }
  return result;
}

bool Tetrahedron::touch( const RealPos & pos, double tol, bool verbose  ) const {
  Node testNode( pos );
  Node *n0 = &node(0), *n1 = &node(1);
  Node *n2 = &node(2), *n3 = &node(3);

  Tetrahedron testTet(*n0, *n1, *n2, *n3);
  //** If the point is on the surface, the point is considered inside.
  double V0 = testTet.volume();

  testTet.setNodes( testNode, *n1, *n2, *n3 );
  double V1 = testTet.volume();

  testTet.setNodes( *n0, testNode, *n2, *n3 );
  double V2 = testTet.volume();

  testTet.setNodes( *n0, *n1, testNode, *n3 );
  double V3 = testTet.volume();

  testTet.setNodes( *n0, *n1, *n2, testNode );
  double V4 = testTet.volume();

  if ( verbose ){
   std::cout << this->id()<< V0 << " " << V1 << " " << V2 << " "
	 	    << V3 << " " << V4 << " sum " << fabs( V0 - V1 - V2 - V3 - V4 ) << std::endl;
  }

  if ( fabs( V0 - V1 - V2 - V3 - V4 ) > tol ) return false;
  return  true;
}


Tetrahedron10::Tetrahedron10( Node & n1, Node & n2, Node & n3, Node & n4, int id, double attribute )
  : Tetrahedron( n1, n2, n3, n4, id, attribute ) {
  for ( int i = 0; i < 6 ; i ++ ) _nodesVector.push_back( NULL );
}

Tetrahedron10::~Tetrahedron10(){
}

void Tetrahedron10::switchDirection(){
  Tetrahedron::switchDirection();
}

// void Tetrahedron10::checkNew(){
//   checkEdge3( 0, 1, 4 );    // edge 0 -- 4 -- 1
//   checkEdge3( 1, 2, 5 );    // edge 1 -- 5 -- 2
//   checkEdge3( 2, 0, 6 );    // edge 2 -- 6 -- 0
//   checkEdge3( 0, 3, 7 );    // edge 0 -- 7 -- 3
//   checkEdge3( 1, 3, 8 );    // edge 1 -- 8 -- 3
//   checkEdge3( 2, 3, 9 );    // edge 2 -- 9 -- 3
// }

void Tetrahedron10::check(){
  checkEdge3( 0, 1, 4 );    // edge 0 -- 4 -- 1
  checkEdge3( 0, 2, 5 );    // edge 0 -- 5 -- 2
  checkEdge3( 0, 3, 6 );    // edge 0 -- 6 -- 3
  checkEdge3( 1, 2, 7 );    // edge 1 -- 7 -- 2
  checkEdge3( 2, 3, 8 );    // edge 2 -- 8 -- 3
  checkEdge3( 1, 3, 9 );    // edge 1 -- 9 -- 3
}

// double Tetrahedron10::partDerivationRealToUnity( char absPos, int relPos ){
//   TO_IMPL
//   return -1.0;
// }
// double Tetrahedron10::jacobianDeterminant( ){
//   TO_IMPL
//   return -1.0;
// }


Hexahedron::Hexahedron( Node & n1, Node & n2, Node & n3, Node & n4, Node & n5, Node & n6, Node & n7, Node & n8,
			int id, double attribute ) : BaseElement() {
  _id = id;
  if ( _attributesVector.size() == 0) _attributesVector.push_back( attribute );
  else _attributesVector[ 0 ] = attribute;
  //    _nodesVector.reserve( 4 );
  _nodesVector.clear();
  _nodesVector.push_back( & n1 ); _nodesVector.push_back( & n2 ); _nodesVector.push_back( & n3 );
  _nodesVector.push_back( & n4 ); _nodesVector.push_back( & n5 ); _nodesVector.push_back( & n6 );
  _nodesVector.push_back( & n7 ); _nodesVector.push_back( & n8 );

}

} // namespace MyMesh

/*
$Log: elements.cpp,v $
Revision 1.40  2010/10/26 12:20:28  carsten
win32 compatibility commit

Revision 1.39  2008/10/14 12:24:46  carsten
*** empty log message ***

Revision 1.38  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.37  2008/03/14 11:16:25  carsten
*** empty log message ***

Revision 1.36  2007/10/22 18:32:08  carsten
*** empty log message ***

Revision 1.35  2007/08/06 12:16:06  carsten
*** empty log message ***

Revision 1.34  2007/04/04 18:35:04  carsten
*** empty log message ***

Revision 1.33  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.32  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.31  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.30  2006/01/29 17:54:05  carsten
*** empty log message ***

Revision 1.29  2005/10/28 16:09:05  carsten
*** empty log message ***

Revision 1.28  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.27  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.26  2005/10/20 16:54:55  carsten
*** empty log message ***

Revision 1.25  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.24  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.23  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.22  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.21  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.20  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.19  2005/10/04 11:07:42  carsten
*** empty log message ***

Revision 1.18  2005/09/28 17:43:20  carsten
*** empty log message ***

Revision 1.17  2005/08/30 21:41:17  carsten
*** empty log message ***

Revision 1.16  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.15  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.14  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.13  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.12  2005/06/13 09:14:46  carsten
*** empty log message ***

Revision 1.11  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.10  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.9  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.8  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.7  2005/03/30 18:37:14  carsten
*** empty log message ***

Revision 1.6  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.5  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.4  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.3  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.2  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

 */
