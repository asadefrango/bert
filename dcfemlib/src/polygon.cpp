// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "polygon.h"
#include "line.h"

ostream & operator << ( ostream & str, const Polygon & poly ){
  str << &poly  << endl;
  if ( poly.marker() != 0 ) cout << "Marker: " << poly.marker() << endl;
  
  for ( size_t i = 0; i < poly.size(); i ++){
    cout << poly[ i ]->id() << "\t";
  }
  cout << endl;
  return str;
}

Polygon::Polygon( Node * n1, Node * n2, int marker ) :  marker_( marker ){
  this->push_back( n1 );
  this->push_back( n2 );
}

Polygon::Polygon( const Polygon & poly ){
  //   : WorldMatrix(){
  for ( size_t i = 0, imax = poly.size(); i < imax; i ++ ){
    this->push_back( poly[ i ] );
  }
  marker_ = poly.marker();
}

void Polygon::switchDirection(){
  Polygon tmp( (*this) );
  this->clear();
  for ( size_t i = 0, imax = tmp.size(); i < imax; i++ ){
    this->push_back( tmp[ imax - 1 - i ] );
  }
//  this->reverse();
}

void Polygon::add( const Polygon & b ){
  //insert( b );
  for ( size_t i = 0, imax = b.size(); i < imax; i ++) this->push_back( b[ i ] );
}

void Polygon::transform( double mat[ 4 ][ 4 ] ){
  for ( size_t i = 0, imax = size(); i < imax; i ++ ) (*this)[ i ]->pos().transform( mat );
}

bool Polygon::contains( Node * node ){
  for ( int i = 0, imax = this->size(); i < imax; i ++ ){
    if ( (*this)[ i ] == node ) return true;
  }
  return false;
}

int Polygon::touch( const RealPos & pos, double tol ){
  //** find the edge where the pos lies exactly, else returns -1;
  bool verbose = false;
  Line edge;
  size_t start = 0, end = 0;
  for ( size_t i = 0, imax = this->size(); i < imax; i ++){
    start = i; 
    if ( start == (imax -1) ) end = 0; else end = i + 1;
    if ( (*this)[ start ]->pos() != (*this)[ end ]->pos() ){

      edge.setPositions( (*this)[ start ]->pos(), (*this)[ end ]->pos() );

      if ( verbose ){
	cout << i << "\t" 
	     << (*this)[ start ]->id() << " = " << (*this)[ start ]->pos()  << "\t"
	     << (*this)[ end ]->id() << " = " << (*this)[ end ]->pos() << "\t"
	     << pos
	     << edge.touch( pos, tol ) << endl;
      }
      if ( edge.touch( pos, tol ) == 3){
	return i;
      }
    }
  }
  return -1;
}

void Polygon::insertNode( Node * node, size_t position ){
  //** sehr haesslich, bedarf umstellung auf list<>;
  Polygon tmp( (*this) );
  this->clear();
  for ( size_t i = 0; i <= position; i++ ){
    this->push_back( tmp[ i ] );
  }
  this->push_back( node );
  for ( size_t i = position + 1; i < tmp.size(); i++ ){
    this->push_back( tmp[ i ] );
  }
}

/*
$Log: polygon.cpp,v $
Revision 1.3  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.2  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
