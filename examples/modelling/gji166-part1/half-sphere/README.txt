Example.

3.3 Discretization for calculating the secondary potential

from

@ARTICLE{RueckerGueSpi2006,
  author = {C. Rücker and T. Günther and K. Spitzer},
  title = {{T}hree-dimensional modelling and inversion of dc resistivity data
    incorporating topography -- {I}. {M}odelling},
  journal = {Geophys. J. Int.},
  year = {2006},
  volume = {166},
  pages = {495--505},
  number = {2},
}

Files:
    calc.sh -- the main calculation script, perform the mesh generation and finite element calculation

    halfsphere.stl -- half sphere geometry in Standard Triangulation Language (STL) file format 
    
    This file is created by the free modelling tool blender (http://www.blender.org/) and exported from there in ascii STL format.
    You can view this file directly with ParaView.
